//
//  BSOffice.swift
//  BS
//
//  Created by Сергей Максимук on 13.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit



class BSOffice: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var name = ""
  var cash = 0.0
  var debt = 0.0
  var turnover = 0.0
  

  override var description: String {
    return "\n\nOffice\n" +
    "name: \(name)\n" +
    "cash: \(cash)\n" +
    "debt: \(debt)\n" +
    "turnover: \(turnover)\n"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    name        <- map["name"]
    cash        <- (map["cash_money"], doubleTransform)
    debt        <- (map["debt_money"], doubleTransform)
    turnover    <- (map["turnover"], doubleTransform)
  }
  
  
}
