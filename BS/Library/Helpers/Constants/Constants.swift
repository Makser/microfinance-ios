//
//  Constants.swift
//  BS
//
//  Created by Сергей Максимук on 12.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation


enum AppURL: String {
  case Domain = "http://crm-mfo.wide-web.spb.ru"
  case AuthToken = "/login"
  case Authorization = "/login_check"
  case Statistic = "/statistics"
  
  
  var url: NSURL? {
    switch self {
    default:  return NSURL(string: AppURL.Domain.rawValue)?.URLByAppendingPathComponent(rawValue)
    }
  }
  
  
}


enum AppColor: Int {
  case color0 = 0x1e254d
  case color1 = 0x3d436a
  case accent = 0xdc2a1b
  case white = 0xffffff
  
  
  var color: UIColor { return UIColor(red: CGFloat((rawValue >> 16) & 0xFF) / 255, green: CGFloat((rawValue >> 8) & 0xFF) / 255, blue: CGFloat(rawValue & 0xFF) / 255, alpha: 1.0) }
  
  
}