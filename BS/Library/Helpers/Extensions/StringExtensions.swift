//
//  StringExtensions.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation


extension String {

  
  func toInt() -> Int? { return Int(self) }
  
  
  func toDouble() -> Double? { return Double(self) }
  
  
  func shortString(length: Int) -> String { return substringToIndex(startIndex.advancedBy(max(0, min(characters.count - 1, length)))) }
  
  
  static func moneyString(money: Double) -> String {
    let numberFormatter = NSNumberFormatter()
    numberFormatter.formatterBehavior = NSNumberFormatterBehavior.Behavior10_4
    numberFormatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
    numberFormatter.locale = NSLocale(localeIdentifier: "ru_RU")
    return numberFormatter.stringFromNumber(NSNumber(double: money))!
  }
  
  
}