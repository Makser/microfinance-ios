//
//  NetworkLayer.swift
//  Framyr
//
//  Created by Сергей Максимук on 09.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit



class NetworkLayer: AFHTTPRequestOperationManager {

  
  
  internal enum RequestMethod: Int {
    case GET, POST, DELETE, PUT, HEAD, PATCH, NONE
  }
  
  
  
  //  MARK: -
  //  MARK: public properties
  var memoCacheSize = 4 * 1024 * 1024;
  var diskCacheSize = 20 * 1024 * 1024;
  let DiskCacheFilePath = "/httpCache"
  
  
  var cacheResults: Bool = false {
    didSet {
      if cacheResults == true {
        let URLCache = NSURLCache(memoryCapacity: memoCacheSize, diskCapacity: diskCacheSize, diskPath: DiskCacheFilePath);
        NSURLCache .setSharedURLCache(URLCache);
      }
      
    }
    
  }
  

  
  //  MARK: -
  //  MARK: public methods
  func request(
    data: [String: AnyObject]?,
    endpoint: String,
    method: RequestMethod,
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) {
      
      self.requestSerializer.timeoutInterval = 15
      
      let parameterData = (data != nil) ? data! : [:];
      
      let operation = request(
        method,
        endpoint: endpoint,
        parameters: parameterData,
        complition: complition
      );
      
      
      
      operation?.start();
  }
  
  
  func request(
    headers: [String: String]?,
    endpoint: String,
    completion: ((response: AnyObject?, error: NSError?)->())?
    ) {
      
      let request = self.requestSerializer.requestWithMethod("GET", URLString: endpoint, parameters: nil, error: nil)
      request.allHTTPHeaderFields = headers
      
      let operation = HTTPRequestOperationWithRequest(
        request,
        success: { (operation, response) -> Void in
          self.callingSuccesses(.NONE, endpoint: endpoint, data: nil, response: response, complition: completion);
        
        }) { (operation, error) -> Void in
          self.callingFails(.NONE, endpoint: endpoint, data: nil, error: error, operation: operation, complition: completion);
        
        }
      operation.start()
  }
  
  
  
  //  MARK: -
  //  MARK: private methods
  private func request(
    method: RequestMethod,
    endpoint: String,
    parameters: [String: AnyObject],
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) -> AFHTTPRequestOperation? {
      
      switch method {
        
      case .GET:
        return requestGETMethod(parameters, endpoint: endpoint, complition: complition);
      case .POST:
        return requestPOSTMethod(parameters, endpoint: endpoint, complition: complition);
      default:
        return nil;
      }
  }
  
  
  private func requestGETMethod(
    data: [String: AnyObject],
    endpoint: String,
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) -> AFHTTPRequestOperation? {
      
      return GET(
        endpoint,
        parameters: data,
        success: { (operation, response) -> Void in
          
          self.callingSuccesses(.GET, endpoint: endpoint, data: data, response: response, complition: complition);
          
        },
        failure: { (operation, error) -> Void in
          
          self.callingFails(.GET, endpoint: endpoint, data: data, error: error, operation: operation, complition: complition);
          
      });
  }
  
  
  private func requestPOSTMethod(
    data: [String: AnyObject],
    endpoint: String,
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) -> AFHTTPRequestOperation? {
      
      return POST(
        endpoint,
        parameters: data,
        success: { (operation, response) -> Void in
          
          self.callingSuccesses(.POST, endpoint: endpoint, data: data, response: response, complition: complition);
          
        },
        failure: { (operation, error) -> Void in
          
          self.callingFails(.POST, endpoint: endpoint, data: data, error: error, operation: operation, complition: complition);
          
      });
  }
  
  
  private func callingSuccesses(
    method: RequestMethod,
    endpoint: String,
    data: [String: AnyObject]?,
    response: AnyObject?,
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) {
      
      if response != nil { setCache(response!, withKey: endpoint) }
      complition?(response: response, error: nil);
  }
  
  
  private func callingFails(
    method: RequestMethod,
    endpoint: String,
    data: [String: AnyObject]?,
    error: NSError?,
    operation: AFHTTPRequestOperation? = nil,
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) {
      
      NSLog(
        "\n\n--- ERROR: \(error?.description)" +
          "\nMETHOD: \(method.rawValue)" +
          "\nENDPOINT: \(endpoint)" +
        "\nDATA: \(data)"
      );
      
      if let error = error {
        if let statusCode = operation?.response?.statusCode {
          MessageCenter.sharedInstance.showError(statusCode, message: error.localizedDescription)
          return
        }
        
        MessageCenter.sharedInstance.showError(error.code, message: error.localizedDescription)
        return
      }
      
      if let cached = getCache(endpoint) as? NSDictionary {
        
        complition?(response: cached, error: error);
      } else {
        
        complition?(response: nil, error: error);
      }
  }
  
  
  
  //  MARK: -
  //  MARK: cache
  private let cachePath: String = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.CachesDirectory, NSSearchPathDomainMask.UserDomainMask, true).first!
  
  
  private func cachePathForKey(key: String) -> String {
    return NSURL(fileURLWithPath: cachePath).URLByAppendingPathComponent(key).absoluteString }
  
  
  private func setCache(value: AnyObject, withKey key: String) {
    let cache = NSKeyedArchiver.archivedDataWithRootObject(value)
    let cachePath = cachePathForKey(key)
    
    let result = cache.writeToFile(cachePath, atomically: true) ? "" : "not "
    
    NSLog("Data for key \'\(key)\' \(result)cached")
  }
  
  
  private func getCache(key: String) -> AnyObject? {
    
    let cachePath = cachePathForKey(key)
    
    if let data = NSData(contentsOfFile: cachePath) {
      
      if let cache = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? NSDictionary {
        
        NSLog("Data for key \'\(key)\' loaded from cache")
        return cache
      }
    }
    
    NSLog("Data for key \'\(key)\' not loaded from cache")
    
    return nil
  }
  
  
  
  //  MARK: -
  //  MARK: cookies
  internal func saveCookies(key: String) -> Bool {
    guard let cookies = NSHTTPCookieStorage.sharedHTTPCookieStorage().cookies else {
      NSLog("No cookies to save"); return false
    }
    
    NSLog("Auth cookies: \(cookies)")
    
    let cookiesData = NSKeyedArchiver.archivedDataWithRootObject(cookies)
    let userDefaults = NSUserDefaults.standardUserDefaults()
    userDefaults.setObject(cookiesData, forKey: key)
    userDefaults.synchronize()
    return true
  }
  
  
  internal func loadCookies(key: String) -> [NSHTTPCookie]? {
    guard let cookiesData = NSUserDefaults.standardUserDefaults().objectForKey(key) as? NSData else {
      NSLog("No cookies to load"); return nil
    }
    guard let cookies = NSKeyedUnarchiver.unarchiveObjectWithData(cookiesData) as? [NSHTTPCookie] else {
      NSLog("Wrong cookie data format"); return nil
    }
    let cookieStorage = NSHTTPCookieStorage.sharedHTTPCookieStorage()
    
    for cookie in cookies {
      cookieStorage.setCookie(cookie)
    }
    
    return cookies
  }
  
  
  internal func clearCookies(key: String) {
    NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
  }
  
  
}
