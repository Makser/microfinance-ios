//
//  BSAPIHelper.swift
//  BS
//
//  Created by Сергей Максимук on 12.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit



class BSAPIHelper: NetworkLayer {

  
  
  //  MARK: -
  //  MARK: constants
  private let kAuthCookieStorageName = "authCookiesStorage"
  
  
  
  //  MARK: -
  //  MARK: properties
  static let sharedInstance = BSAPIHelper()
  
  
  var haveCookies: Bool { return NSUserDefaults.standardUserDefaults().objectForKey(kAuthCookieStorageName) != nil }
  
  
  
  //  MARK: -
  //  MARK: methods
  func signIn(login: String, pass: String, completion: (NSError?)->()) {
    if let url = AppURL.Authorization.url {
     
      getAuthorizationToken({ (token) -> () in
        guard let token = token else {
          let errorEmptyToken = NSError(domain: NSCocoaErrorDomain, code: 1234, userInfo: [
            NSLocalizedDescriptionKey : NSLocalizedString("_crm_wrong_token", comment: "")
            ])
          completion(errorEmptyToken)
          return
        }
        
        
        
        let params = [
          "_username" : login,
          "_password" : pass,
          "_csrf_token" : token,
          "_submit" : "Войти"
        ]
        
        self.responseSerializer = AFHTTPResponseSerializer()
        self.request(params, endpoint: url.absoluteString, method: .POST, complition: { (response, error) -> () in
          if self.saveCookies(self.kAuthCookieStorageName) {
            completion(error)
          
          } else {
            let errorEmptyCookies = NSError(domain: NSCocoaErrorDomain, code: 1235, userInfo: [
              NSLocalizedDescriptionKey : NSLocalizedString("_crm_wrong_cookies", comment: "")
              ])
            completion(errorEmptyCookies)
          }
        })
        
      })
    }
    
  }
  
  
  func getStatistic(completion: ([BSOffice], NSError?)->()) {
    guard let url = AppURL.Statistic.url else {
      completion([], nil); return
    }
    
    guard let cookies = loadCookies(kAuthCookieStorageName) else {
      NSLog("No cookies for GET request"); completion([], nil); return
    }
    
    let headers = NSHTTPCookie.requestHeaderFieldsWithCookies(cookies)
    

    self.responseSerializer = AFJSONResponseSerializer()
    self.request(headers, endpoint: url.absoluteString) { (response, error) -> () in
      guard let data = response?["offices"] as? NSArray else {
        completion([], error); return
      }
      
      NSLog("\(data)")
      
      guard let offices = Mapper<BSOffice>().mapArray(data) else {
        completion([], error); return
      }
      
      completion(offices, error)
    }
    
  }
  
  
  func getAuthorizationToken(completion: (token: String?)->()) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
      if let url = AppURL.AuthToken.url {
        do {
          let html = try String(contentsOfURL: url)
          let token = self.parseToken(html)
          completion(token: token)
          
        } catch {
          completion(token: nil)
          
        }
      }
      
    })
  
  }
  
  
  
  //  MARK: -
  //  MARK: Cookies
  func clearCookies() {
    clearCookies(kAuthCookieStorageName)
  }
  
  
  
  //  MARK: -
  //  MARK: addition
  private func parseToken(input: String) -> String? {
    let tokenPattern = "\\s*name\\s*=\\s*\"_csrf_token\"\\s*value\\s*=\\s*\"([\\w\\d-]+)\""
    
    do {
      let tokenRegex = try NSRegularExpression(pattern: tokenPattern, options: NSRegularExpressionOptions.CaseInsensitive)
      
      let tokenMatches = tokenRegex.matchesInString(input, options: [], range: NSMakeRange(0, input.characters.count))
      for match in tokenMatches {
        let token = (input as NSString).substringWithRange (match.rangeAtIndex(1))
        
        return token
      }
    }
    catch {
    
    }
    
    return nil
  }
  
  
}
