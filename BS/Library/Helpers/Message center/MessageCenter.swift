//
//  MessageCenter.swift
//  EKOTribe
//
//  Created by Сергей Максимук on 29.10.15.
//  Copyright (c) 2015 wide-web. All rights reserved.
//

import UIKit


class MessageCenter: NSObject {
  
  
  
  //  MARK: -
  //  MARK: properties
  static let sharedInstance = MessageCenter()
  
  private var _systemAlert: UIAlertView?
  
  
  
  //  MARK: -
  //  MARK: methods
  func showWait(message: String) {  }
  func showMessage(message: String) { systemAlert(message: message) }
  func showWarning(message: String) { systemAlert(message: message) }
  func showSuccess(message: String) { systemAlert(message: message) }
  func showError(code: Int? = nil, message: String) {
    systemAlert(
      NSLocalizedString("Error", comment: "") + " " + (code != nil ? "\(code!)" : "") ,
      message: message) }
  
  func hideAlert() {}
  

}



//  MARK: -
//  MARK: system alert
extension MessageCenter: UIAlertViewDelegate {
  
  
  private func systemAlert(
    title: String = "",
    message: String = "",
    cancelTitle: String = NSLocalizedString("OK", comment: ""),
    cancelBlock: (()->())? = nil,
    okTitle: String? = nil,
    okBlock: (()->())? = nil
  ) {
    dispatch_async(dispatch_get_main_queue(), { () -> Void in
      if let okTitle = okTitle {
        self._systemAlert = UIAlertView(
          title: title,
          message: message,
          delegate: self,
          cancelButtonTitle: cancelTitle,
          otherButtonTitles: okTitle)
      } else {
        self._systemAlert = UIAlertView(
          title: title,
          message: message,
          delegate: self,
          cancelButtonTitle: cancelTitle)
      }
      
      self._systemAlert?.show()
    })

  }
  
  
}