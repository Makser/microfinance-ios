//
//  Singletone.swift
//  BS
//
//  Created by Сергей Максимук on 13.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation


class Singletone {

  
  
  //  MARK: -
  //  MARK: properties
  static let sharedinstance = Singletone()
  
  
  var offices = [BSOffice]()
  
  
  
  //  MARK: -
  //  MARK: methods
  func updateOfficeStatistic(completion: ()->()) {
    BSAPIHelper.sharedInstance.getStatistic({ (offices, error) -> () in
      self.offices = offices

      completion()
    })
    
  }
  
  
}