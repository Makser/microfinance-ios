//
//  BSBaseViewController.swift
//  BS
//
//  Created by Сергей Максимук on 12.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class BSBaseViewController: UIViewController {

}



//  MARK: -
//  MARK: VLC
extension BSBaseViewController {


  override func viewDidLoad() {
    super.viewDidLoad()
    
    commonPreparation()
  }
  
  
}



//  MARK: -
//  MARK: preparation
extension BSBaseViewController {
  

  func commonPreparation() {
    view.backgroundColor = AppColor.color1.color
  }
  
  
}