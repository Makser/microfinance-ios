//
//  AppDelegate.swift
//  BS
//
//  Created by Сергей Максимук on 12.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    // Override point for customization after application launch.
    UIApplication.sharedApplication().statusBarStyle = .LightContent
    
    Fabric.with([Crashlytics.self])
    return true
  }


}

