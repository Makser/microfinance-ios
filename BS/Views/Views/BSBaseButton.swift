//
//  BSBaseButton.swift
//  BS
//
//  Created by Сергей Максимук on 12.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class BSBaseButton: UIButton {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    layer.borderColor = AppColor.accent.color.CGColor
    layer.cornerRadius = 2
    layer.borderWidth = 1
    backgroundColor = UIColor.clearColor()
    setTitleColor(AppColor.accent.color, forState: .Normal)
  }
  

}


class BSDownArrowButton: UIButton {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    tintColor = AppColor.accent.color
    
    titleLabel?.font = UIFont(name: kFontAwesomeFamilyName, size: 20)
    titleLabel?.textAlignment = .Center
    setTitle(NSString.fontAwesomeIconStringForEnum(.FAIconArrowDown), forState: .Normal)
  }
  
  
}


class BSSignOutButton: UIButton {
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    tintColor = AppColor.accent.color
    
    titleLabel?.font = UIFont(name: kFontAwesomeFamilyName, size: 20)
    titleLabel?.textAlignment = .Center
    setTitle(NSString.fontAwesomeIconStringForEnum(.FAIconSignout), forState: .Normal)
  }
  
  
}