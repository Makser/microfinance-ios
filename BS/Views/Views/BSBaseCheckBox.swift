//
//  BSBaseCheckBox.swift
//  BS
//
//  Created by Сергей Максимук on 13.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class BSBaseCheckBox: BEMCheckBox {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    onTintColor = AppColor.white.color
    onCheckColor = AppColor.white.color
  }
  
  
}
