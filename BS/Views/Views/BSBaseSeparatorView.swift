//
//  BSBaseSeparatorView.swift
//  BS
//
//  Created by Сергей Максимук on 13.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class BSBaseSeparatorView: UIView {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    backgroundColor = AppColor.color0.color
  }
  

}
