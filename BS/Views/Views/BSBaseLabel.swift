//
//  BSBaseLabel.swift
//  BS
//
//  Created by Сергей Максимук on 13.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit



class BSBaseLabel: UILabel {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    textColor = AppColor.white.color
  }
  
  
}


class BSH1Label: BSBaseLabel {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    font = font.fontWithSize(18)
  }
  
  
}


class BSH2Label: BSBaseLabel {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    font = font.fontWithSize(16)
  }
  
  
}


class BSH3Label: BSBaseLabel {
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    font = font.fontWithSize(14)
  }
  
  
}


class BSContentLabel: BSBaseLabel {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    font = font.fontWithSize(12)
    textAlignment = .Center
    
    layer.borderWidth = 1
    layer.borderColor = AppColor.color1.color.CGColor
    
    backgroundColor = AppColor.white.color
    textColor = AppColor.color0.color
  }
  
  
}