//
//  BSStatisticTableViewCell.swift
//  BS
//
//  Created by Сергей Максимук on 13.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class BSStatisticTableViewCell: UITableViewCell {

  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var nameLabel: UILabel!;
  @IBOutlet private weak var cashLabel: UILabel!;
  @IBOutlet private weak var debtLabel: UILabel!;
  @IBOutlet private weak var turnoverLabel: UILabel!;
  
  
  
  //  MARK: -
  //  MARK: methods
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    
  }
  
  
  override func setSelected(selected: Bool, animated: Bool) {
    highlight(selected)
  }
  
  
  override func setHighlighted(highlighted: Bool, animated: Bool) {
    highlight(highlighted)
  }
  
  
  func setData(data: BSOffice) {
    nameLabel.text = data.name
    cashLabel.text = String.moneyString(data.cash)
    debtLabel.text = String.moneyString(data.debt)
    turnoverLabel.text = String.moneyString(data.turnover)
  }
  
  
  private func highlight(highlight: Bool) {
    
  }
  
  
}