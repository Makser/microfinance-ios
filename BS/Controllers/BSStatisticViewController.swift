//
//  BSStatisticViewController.swift
//  BS
//
//  Created by Сергей Максимук on 13.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class BSStatisticViewController: BSBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  private var items = [BSOffice] ()
    {
    didSet {
      var cash = 0.0
      var debt = 0.0
      var turnover = 0.0
      
      for item in items {
        cash += item.cash
        debt += item.debt
        turnover += item.turnover
      }
      
//      var total = items.reduce( (cash: 0.0, debt: 0.0, turnover: 0.0) ) { (res, item) -> (Double, Double, Double) in
//        return ( res.cash + item.cash, res.debt + item.debt, res.turnover + item.turnover )
//      }
      
      cashLabel?.text = String.moneyString(cash)
      debtLabel?.text = String.moneyString(debt)
      turnoverLabel?.text = String.moneyString(turnover)
      
      tableView?.reloadData()
    }
  }
  

  private var onceToken: dispatch_once_t = 0
  private var refreshControl: UIRefreshControl!
  
  
  @IBOutlet private weak var cashLabel: UILabel?;
  @IBOutlet private weak var debtLabel: UILabel?;
  @IBOutlet private weak var turnoverLabel: UILabel?;
  @IBOutlet private weak var tableView: UITableView?;
  @IBOutlet private weak var bottomTableConstraint: NSLayoutConstraint!;
  @IBOutlet private weak var switchTableButton: UIButton!;
  
  
  
  //  MARK: -
  //  MARK: constants
  private let kCellHeight: CGFloat = 100
  
  
}



//  MARK: -
//  MARK: VLC
extension BSStatisticViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    addPullToRefresh()
    refreshData()
    tableView?.hidden = true
    
    if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
      tableView?.rowHeight = UITableViewAutomaticDimension
      tableView?.estimatedRowHeight = kCellHeight
    }
    
  }
  
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    dispatch_once(&onceToken) { () -> Void in
      self.hideTable()
    }
  }
  
  
}



//  MARK: -
//  MARK: data
extension BSStatisticViewController {
  
  
  func loadData() {
    items = Singletone.sharedinstance.offices
  }
  
  
  func refreshData() {
    Singletone.sharedinstance.updateOfficeStatistic { () -> () in
      self.refreshControl.endRefreshing()
      self.loadData()
    }
    
  }
  
  
  
}



//  MARK: -
//  MARK: pull to refresh
extension BSStatisticViewController {
  
  
  func addPullToRefresh() {
    refreshControl = UIRefreshControl()
    refreshControl.tintColor = AppColor.white.color
    refreshControl.addTarget(self, action: Selector("refresh:"), forControlEvents: .ValueChanged)
    tableView?.addSubview(refreshControl)
  }
  
  
  func refresh(sender: AnyObject) {
    refreshData()
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension BSStatisticViewController: UITableViewDataSource {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(Reusable.cellStatistic, forIndexPath: indexPath)
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension BSStatisticViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? BSStatisticTableViewCell {
      let item = items[indexPath.row]
      cell.setData(item)
      
    }
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
      return UITableViewAutomaticDimension
    } else {
      return kCellHeight
    }
    
  }
  
  
}



//  MARK: -
//  MARK: actions
extension BSStatisticViewController {
  
  
  @IBAction func showHideTable(sender: AnyObject) {
    switchTable()
  }
  
  
  @IBAction func signOutTapped(sender: AnyObject) {
    BSAPIHelper.sharedInstance.clearCookies()
    navigationController?.popViewControllerAnimated(true)
  }
  
  
}



//  MARK: -
//  MARK: animation
extension BSStatisticViewController {
  
  
  func switchTable() {
    guard let tableView = self.tableView else { return }

    tableView.hidden ? showTable() : hideTable()
  }
  
  
  func showTable() {
    view.layoutIfNeeded()
    self.bottomTableConstraint.constant = 0
    tableView?.hidden = false
    UIView.animateWithDuration(
      0.7,
      animations: { () -> Void in
        self.switchTableButton.transform = CGAffineTransformMakeRotation(CGFloat(M_PI))
        self.view.layoutIfNeeded()
      }) { (_) -> Void in
        
      }

  }
  

  func hideTable() {
    view.layoutIfNeeded()
    self.bottomTableConstraint.constant = tableView!.frame.size.height
    UIView.animateWithDuration(
      0.7,
      animations: { () -> Void in
        self.switchTableButton.transform = CGAffineTransformIdentity
        self.view.layoutIfNeeded()
      }) { (_) -> Void in
        self.tableView?.hidden = true
      }
    
  }
  
  
}