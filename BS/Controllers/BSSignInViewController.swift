//
//  BSSignInViewController.swift
//  BS
//
//  Created by Сергей Максимук on 12.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class BSSignInViewController: BSBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  private let kRememberMeStorageName = "kRememberMe"
  private var rememberMe: Bool {
    get { return NSUserDefaults.standardUserDefaults().boolForKey(kRememberMeStorageName) }
    set {
      NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: kRememberMeStorageName)
      NSUserDefaults.standardUserDefaults().synchronize()
    }
  }
  
  
  private var onceToken: dispatch_once_t = 0
  private var currentTextfield: UITextField?
  
  
  @IBOutlet private weak var loginField: UITextField!;
  @IBOutlet private weak var passField: UITextField!;
  @IBOutlet private weak var enterButton: UIButton!;
  @IBOutlet private weak var activity: UIActivityIndicatorView!;
  
  
}



//  MARK: -
//  MARK: VLC
extension BSSignInViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupKeyboardNotifications()
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    loginField.text = ""
    passField.text = ""
  }
  
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    dispatch_once(&onceToken) { () -> Void in
      if self.rememberMe && BSAPIHelper.sharedInstance.haveCookies {
        self.performSegue(Segue.segueSignInToStatistic)
      }
    }
    
  }
  
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    
    currentTextfield?.resignFirstResponder()
  }
  
  
}



//  MARK: -
//  MARK: actions
extension BSSignInViewController {
  
  
  @IBAction func enterTapped(sender: AnyObject) {
    guard let login = loginField.text, let pass = passField.text else {
      return
    }
    
    
    if login.characters.count == 0 {
      MessageCenter.sharedInstance.showError(message: NSLocalizedString("_signin_no_login", comment: "")); return
    }
    if pass.characters.count == 0 {
      MessageCenter.sharedInstance.showError(message: NSLocalizedString("_signin_no_pass", comment: "")); return
    }
    
    
    activity.startAnimating(); enterButton.hidden = true
    BSAPIHelper.sharedInstance.signIn(login, pass: pass) { (error) -> () in
      self.activity.stopAnimating(); self.enterButton.hidden = false
      
//      if let error = error {
//        MessageCenter.sharedInstance.showError(error.localizedDescription)
//        return
//      }
      
      if error == nil {
        self.performSegue(Segue.segueSignInToStatistic)
      }
      
    }
    
  }

  
}



//  MARK: -
//  MARK: keyboard appearance
extension BSSignInViewController {
  
  
  func setupKeyboardNotifications() {
    NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name: UIKeyboardWillHideNotification, object: nil)
  }
  
  
  func keyboardWillShow(notification: NSNotification) {
    let keyboardFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
    
    
    UIView.animateWithDuration(0.7) { () -> Void in
      self.view.frame.size.height = UIScreen.mainScreen().bounds.size.height - keyboardFrame.size.height
      self.view.layoutIfNeeded()
    }
  }
  
  
  func keyboardWillHide(notification: NSNotification) {
    UIView.animateWithDuration(0.7) { () -> Void in
      self.view.frame.size.height = UIScreen.mainScreen().bounds.size.height
    }
  }
  
  
}



//  MARK: -
//  MARK: UITextFieldDelegate
extension BSSignInViewController: UITextFieldDelegate {
  
  
  func textFieldDidBeginEditing(textField: UITextField) {
    currentTextfield = textField
  }
  
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  
}



//  MARK: -
//  MARK: BEMCheckBoxDelegate
extension BSSignInViewController: BEMCheckBoxDelegate {
  
  
  func didTapCheckBox(checkBox: BEMCheckBox) {
    rememberMe = checkBox.on
  }
  
  
}